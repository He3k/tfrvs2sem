#include <iostream>
#include <sstream>

int main(int argc, char *argv[]) {
    if (argc != 4) {
        std::cerr << "Применение: " << argv[0] << " <λ> <µ> <m>\n";
        return 1;
    }
    double lambda, m, u;
    std::stringstream ss;
    ss << argv[1];
    ss >> lambda;
    ss.clear();
    ss << argv[2];
    ss >> u;
    ss.clear();
    ss << argv[3];
    ss >> m;
    ss.clear();
    
    double N = 65536;

    for (int n = 65527; n <= N; n++) {
        double operatingTime = 0;
        if (n != N) {
            for (double j = n + 1; j <= N; j++) {
                double resultOfMultiplication = 1;
                for (double l = n; l <= j - 1; l++) {
                    double ul = 0;
                    if (N - m <= l && l <= N) {
                        ul = (N - l) * u;
                    } else if (0 <= l && l < N - m) {
                        ul = m * u;
                    } else {
                        std::cout << "Error" << std::endl;
                    }

                    resultOfMultiplication *= (ul / (l * lambda));
                }
                operatingTime += (1.0 / (j * lambda) * resultOfMultiplication + 1 / (n * lambda));
            }
        } else if (n == N) {
            operatingTime = 1.0 / N / lambda;
        }

        double averageRecoveryTime = 0;
        double u0 = 1;
        if (n == 1) {
            averageRecoveryTime = 1 / u0;
        } else if (n > 1) {
            averageRecoveryTime = 1 / u0;
            double ul = 0;
            for (double l = 1; l <= n - 1; l++) {
                if (N - m <= l && l <= N) {
                    ul = (N - l) * u;
                } else if (0 <= l && l < N - m) {
                    ul = m * u;
                } else {
                    std::cout << "Error" << std::endl;
                }

                averageRecoveryTime *= (l * lambda / ul);
            }

            for (double j = 1; j <= n - 1; j++) {
                double resultOfMultiplication = 1;
                for (double l = j; l <= n - 1; l++) {
                    if (N - m <= l && l <= N) {
                        ul = (N - l) * u;
                    } else if (0 <= l && l < N - m) {
                        ul = m * u;
                    } else {
                        std::cout << "Error" << std::endl;
                    }
                    resultOfMultiplication *= (l * lambda / ul);
                }
                averageRecoveryTime += (1 / (j * lambda)) * resultOfMultiplication;
            }
        } else {
            std::cout << "Error" << std::endl;
        }
        std::cout << n << " " << operatingTime << " " << averageRecoveryTime << std::endl;
    }

    return 0;
}
