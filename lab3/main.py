import matplotlib.pyplot as plt
from time import time
import subprocess, os, sys

def main(argc):
    compile_command = "g++ main.cpp -o main"
    subprocess.run(compile_command, shell=True, check=True)

    lambda_ = [10**-5, 10**-6, 10**-7]
    mu = [10**0, 10**1, 10**2, 10**3]
    m = [1, 2, 3]

    up_lambda, down_lambda, up_mu, down_mu, up_m, down_m = [], [], [], [], [], []

    if argc[1] == "lambda" or argc[1] == "all":
        # res1 = [re.split('[\n ]', subprocess.run(["./main"] + [str(l), str(mu[0]), str(m[0])], stdout=subprocess.PIPE, text=True).stdout)[:-1] for l in lambda_]
        # res1_flatt = [float(num) for li in res1 for num in li]
        # up_lambda = [[res1_flatt[i], res1_flatt[i+1]] for i in range(0, len(res1_flatt), 3)]
        up_lambda = [[65527.0, 207.968], [65528.0, 137.957], [65529.0, 91.557], [65530.0, 60.6265], [65531.0, 39.8325], [65532.0, 25.6806], [65533.0, 15.8809], [65534.0, 8.93314], [65535.0, 3.85424], [65536.0, 1.52588], [65527.0, 732717000000.0], [65528.0, 48012800000.0], [65529.0, 3146180000.0], [65530.0, 206166000.0], [65531.0, 13510100.0], [65532.0, 885372.0], [65533.0, 58046.7], [65534.0, 3816.24], [65535.0, 248.093], [65536.0, 15.2588], [65527.0, 6.89215e+21], [65528.0, 4.51622e+19], [65529.0, 2.95939e+17], [65530.0, 1939260000000000.0], [65531.0, 12707900000000.0], [65532.0, 83276400000.0], [65533.0, 545728000.0], [65534.0, 3576470.0], [65535.0, 23436.0], [65536.0, 152.588]]
        fig, ax = plt.subplots()
        for i in range(0, len(up_lambda), len(up_lambda)//len(lambda_)):
            plt.plot([i[0] for i in up_lambda[i:i+len(up_lambda)//len(lambda_)]], [i[1] for i in up_lambda[i:i+len(up_lambda)//len(lambda_)]], label=f"λ = {lambda_[i//10]} hours^-1")
        plt.title("График среднего времени безотказной работы при μ = 1 hours^-1, m = 1", fontsize=10)
        ax.set_xlabel("Количество элементарных машин (n)", fontsize=10)
        ax.set_ylabel("Среднее время безотказной работы (ч)", fontsize=10)
        ax.legend()
        ax.grid(True)
        plt.yscale('log')
        fig.savefig(f"1.png", dpi=100, bbox_inches='tight')
        plt.close(fig)

        fig, ax = plt.subplots()
        # down_lambda = [[res1_flatt[i], res1_flatt[i+2]] for i in range(0, len(res1_flatt), 3)]
        down_lambda = [[65527.0, 2.90058], [65528.0, 2.90066], [65529.0, 2.90075], [65530.0, 2.90083], [65531.0, 2.90091], [65532.0, 2.901], [65533.0, 2.90108], [65534.0, 2.90117], [65535.0, 2.90125], [65536.0, 2.90133], [65527.0, 1.07012], [65528.0, 1.07012], [65529.0, 1.07012], [65530.0, 1.07012], [65531.0, 1.07013], [65532.0, 1.07013], [65533.0, 1.07013], [65534.0, 1.07013], [65535.0, 1.07013], [65536.0, 1.07013], [65527.0, 1.0066], [65528.0, 1.0066], [65529.0, 1.0066], [65530.0, 1.0066], [65531.0, 1.0066], [65532.0, 1.0066], [65533.0, 1.0066], [65534.0, 1.0066], [65535.0, 1.0066], [65536.0, 1.0066]]
        for i in range(0, len(down_lambda), len(down_lambda)//len(lambda_)):
            plt.plot([i[0] for i in down_lambda[i:i+len(down_lambda)//len(lambda_)]], [i[1] for i in down_lambda[i:i+len(down_lambda)//len(lambda_)]], label=f"λ = {lambda_[i//10]} hours^-1")
        plt.title("График среднего времени восстановления ВС при μ = 1 hours^-1, m = 1", fontsize=10)
        ax.set_xlabel("Количество элементарных машин (n)", fontsize=10)
        ax.set_ylabel("Среднее времени восстановления ВС (ч)", fontsize=10)
        ax.legend()
        ax.grid(True)
        plt.yscale('log')
        fig.savefig(f"2.png", dpi=100, bbox_inches='tight')
        plt.close(fig)

        # print(up_lambda)
        # print(down_lambda)

    if argc[1] == "mu" or argc[1] == "all":
        # res2 = [re.split('[\n ]', subprocess.run(["./main"] + [str(lambda_[0]), str(mu_), str(m[0])], stdout=subprocess.PIPE, text=True).stdout)[:-1] for mu_ in mu]
        # res2_flatt = [float(num) for li in res2 for num in li]
        
        fig, ax = plt.subplots()
        # up_mu = [[res2_flatt[i], res2_flatt[i+1]] for i in range(0, len(res2_flatt), 3)]
        up_mu = [[65527.0, 207.968], [65528.0, 137.957], [65529.0, 91.557], [65530.0, 60.6265], [65531.0, 39.8325], [65532.0, 25.6806], [65533.0, 15.8809], [65534.0, 8.93314], [65535.0, 3.85424], [65536.0, 1.52588], [65527.0, 73271700000.0], [65528.0, 4801280000.0], [65529.0, 314618000.0], [65530.0, 20616600.0], [65531.0, 1351010.0], [65532.0, 88537.2], [65533.0, 5804.67], [65534.0, 381.624], [65535.0, 24.8093], [65536.0, 1.52588], [65527.0, 6.89215e+19], [65528.0, 4.51622e+17], [65529.0, 2959390000000000.0], [65530.0, 19392600000000.0], [65531.0, 127079000000.0], [65532.0, 832764000.0], [65533.0, 5457280.0], [65534.0, 35764.7], [65535.0, 234.36], [65536.0, 1.52588], [65527.0, 6.85147e+28], [65528.0, 4.48956e+25], [65529.0, 2.94192e+22], [65530.0, 1.92781e+19], [65531.0, 1.26329e+16], [65532.0, 8278490000000.0], [65533.0, 5425060000.0], [65534.0, 3555210.0], [65535.0, 2329.87], [65536.0, 1.52588]]
        for i in range(0, len(up_mu), len(up_mu)//len(mu)):
            plt.plot([i[0] for i in up_mu[i:i+len(up_mu)//len(mu)]], [i[1] for i in up_mu[i:i+len(up_mu)//len(mu)]], label=f"μ = {mu[i//10]} hours^-1")
        plt.title(f"График среднего времени безотказной работы при λ = {lambda_[0]} hours^-1, m = {m[0]}", fontsize=10)
        ax.set_xlabel("Количество элементарных машин (n)", fontsize=10)
        ax.set_ylabel("Среднее времени безотказной работы ВС (ч)", fontsize=10)
        ax.legend()
        ax.grid(True)
        plt.yscale('log')
        fig.savefig(f"3.png", dpi=100, bbox_inches='tight')
        plt.close(fig)

        fig, ax = plt.subplots()
        # down_mu = [[res2_flatt[i], res2_flatt[i+2]] for i in range(0, len(res2_flatt), 3)]
        down_mu = [[65527.0, 2.90058], [65528.0, 2.90066], [65529.0, 2.90075], [65530.0, 2.90083], [65531.0, 2.90091], [65532.0, 2.901], [65533.0, 2.90108], [65534.0, 2.90117], [65535.0, 2.90125], [65536.0, 2.90133], [65527.0, 0.107012], [65528.0, 0.107012], [65529.0, 0.107012], [65530.0, 0.107012], [65531.0, 0.107013], [65532.0, 0.107013], [65533.0, 0.107013], [65534.0, 0.107013], [65535.0, 0.107013], [65536.0, 0.107013], [65527.0, 0.010066], [65528.0, 0.010066], [65529.0, 0.010066], [65530.0, 0.010066], [65531.0, 0.010066], [65532.0, 0.010066], [65533.0, 0.010066], [65534.0, 0.010066], [65535.0, 0.010066], [65536.0, 0.010066], [65527.0, 0.00100066], [65528.0, 0.00100066], [65529.0, 0.00100066], [65530.0, 0.00100066], [65531.0, 0.00100066], [65532.0, 0.00100066], [65533.0, 0.00100066], [65534.0, 0.00100066], [65535.0, 0.00100066], [65536.0, 0.00100066]]
        for i in range(0, len(down_mu), len(down_mu)//len(mu)):
            plt.plot([i[0] for i in down_mu[i:i+len(down_mu)//len(mu)]], [i[1] for i in down_mu[i:i+len(down_mu)//len(mu)]], label=f"μ = {mu[i//10]} hours^-1")
        plt.title(f"График среднего времени восстановления ВС при λ = {lambda_[0]} hours^-1, m = {m[0]}", fontsize=10)
        ax.set_xlabel("Количество элементарных машин (n)", fontsize=10)
        ax.set_ylabel("Среднее времени восстановления ВС (ч)", fontsize=10)
        ax.legend()
        ax.grid(True)
        # plt.yscale('log')
        fig.savefig(f"4.png", dpi=100, bbox_inches='tight')
        plt.close(fig)

        # print(up_mu)
        # print(down_mu)

    if argc[1] == "m" or argc[1] == "all":
        # res3 = [re.split('[\n ]', subprocess.run(["./main"] + [str(lambda_[0]), str(mu[1]), str(m_)], stdout=subprocess.PIPE, text=True).stdout)[:-1] for m_ in m]
        # res3_flatt = [float(num) for li in res3 for num in li]
        fig, ax = plt.subplots()
        # up_m = [[res3_flatt[i], res3_flatt[i+1]] for i in range(0, len(res3_flatt), 3)]
        up_m = [[65527.0, 73271700000.0], [65528.0, 4801280000.0], [65529.0, 314618000.0], [65530.0, 20616600.0], [65531.0, 1351010.0], [65532.0, 88537.2], [65533.0, 5804.67], [65534.0, 381.624], [65535.0, 24.8093], [65536.0, 1.52588], [65527.0, 18715900000000.0], [65528.0, 613199000000.0], [65529.0, 20090800000.0], [65530.0, 658266000.0], [65531.0, 21568100.0], [65532.0, 706694.0], [65533.0, 23158.4], [65534.0, 760.195], [65535.0, 24.8093], [65536.0, 1.52588], [65527.0, 319771000000000.0], [65528.0, 6984560000000.0], [65529.0, 152561000000.0], [65530.0, 3332400000.0], [65531.0, 72790700.0], [65532.0, 1590020.0], [65533.0, 34735.3], [65534.0, 760.195], [65535.0, 24.8093], [65536.0, 1.52588]]
        for i in range(0, len(up_m), len(up_m)//len(m)):
            plt.plot([i[0] for i in up_m[i:i+len(up_m)//len(m)]], [i[1] for i in up_m[i:i+len(up_m)//len(m)]], label=f"m = {m[i//10]}")
        plt.title(f"График среднего времени безотказной работы при λ = {lambda_[0]} hours^-1, μ = {m[0]} hours^-1", fontsize=10)
        ax.set_xlabel("Количество элементарных машин (n)", fontsize=10)
        ax.set_ylabel("Среднее времени безотказной работы ВС (ч)", fontsize=10)
        ax.legend()
        ax.grid(True)
        plt.yscale('log')
        fig.savefig(f"5.png", dpi=100, bbox_inches='tight')
        plt.close(fig)

        fig, ax = plt.subplots()
        # down_m = [[res3_flatt[i], res3_flatt[i+2]] for i in range(0, len(res3_flatt), 3)]
        down_m = [[65527.0, 0.107012], [65528.0, 0.107012], [65529.0, 0.107012], [65530.0, 0.107012], [65531.0, 0.107013], [65532.0, 0.107013], [65533.0, 0.107013], [65534.0, 0.107013], [65535.0, 0.107013], [65536.0, 0.107013], [65527.0, 0.0516936], [65528.0, 0.0516937], [65529.0, 0.0516937], [65530.0, 0.0516937], [65531.0, 0.0516937], [65532.0, 0.0516938], [65533.0, 0.0516938], [65534.0, 0.0516938], [65535.0, 0.0516939], [65536.0, 0.103388], [65527.0, 0.0340777], [65528.0, 0.0340777], [65529.0, 0.0340777], [65530.0, 0.0340777], [65531.0, 0.0340777], [65532.0, 0.0340777], [65533.0, 0.0340777], [65534.0, 0.0340777], [65535.0, 0.0511166], [65536.0, 0.10335]]
        for i in range(0, len(down_m), len(down_m)//len(m)):
            plt.plot([i[0] for i in down_m[i:i+len(down_m)//len(m)]], [i[1] for i in down_m[i:i+len(down_m)//len(m)]], label=f"m = {m[i//10]}")
        plt.title(f"График среднего времени восстановления ВС при λ = {lambda_[0]} hours^-1, μ = {m[0]} hours^-1", fontsize=10)
        ax.set_xlabel("Количество элементарных машин (n)", fontsize=10)
        ax.set_ylabel("Среднее времени восстановления ВС (ч)", fontsize=10)
        ax.legend()
        ax.grid(True)
        # plt.yscale('log')
        fig.savefig(f"6.png", dpi=100, bbox_inches='tight')
        plt.close(fig)

        # print(up_m)
        # print(down_m)
    os.remove("main")

    arr1 = ", ".join([str(l) for l in lambda_]) + "; " + str(mu[0]) + "; " + str(m[0]) + "; " + ", ".join([str(i) for i in range(65527, 65536+1)]) + "; " + ", ".join([str(upl[1]) for upl in up_lambda])
    arr2 = ", ".join([str(l) for l in lambda_]) + "; " + str(mu[0]) + "; " + str(m[0]) + "; " + ", ".join([str(i) for i in range(65527, 65536+1)]) + "; " + ", ".join([str(dpl[1]) for dpl in down_lambda])
    arr3 = str(lambda_[0]) + "; " + ", ".join([str(i) for i in mu]) + "; " + str(m[0]) + "; " + ", ".join([str(i) for i in range(65527, 65536+1)]) + "; " + ", ".join([str(umu[1]) for umu in up_mu])
    arr4 = str(lambda_[0]) + "; " + ", ".join([str(i) for i in mu]) + "; " + str(m[0]) + "; " + ", ".join([str(i) for i in range(65527, 65536+1)]) + "; " + ", ".join([str(dmu[1]) for dmu in down_mu])
    arr5 = str(lambda_[0]) + "; " + str(mu[0]) + "; " + ", ".join([str(i) for i in m]) + "; " + ", ".join([str(i) for i in range(65527, 65536+1)]) + "; " + ", ".join([str(um[1]) for um in up_m])
    arr6 = str(lambda_[0]) + "; " + str(mu[0]) + "; " + ", ".join([str(i) for i in m]) + "; " + ", ".join([str(i) for i in range(65527, 65536+1)]) + "; " + ", ".join([str(dm[1]) for dm in down_m])
    with open("results.dat", "w+") as file:
        file.write("λ     µ     m     n     θ     T\n")
        file.write(f"{arr1}\n{arr2}\n{arr3}\n{arr4}\n{arr5}\n{arr6}")
    file.close()

if __name__ == "__main__":
    # start = time()
    if len(sys.argv) != 2:
        print("Применение: python main.py <lambda/mu/m/all>")
    else:
        if sys.argv[1] in ["lambda", "mu", "m", "all"]:
            main(sys.argv)
        else:
            print("Применение: python main.py <lambda/mu/m/all>")
    # print(f"Работа программы заняла {time()-start} секунд.")