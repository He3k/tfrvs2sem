from sys import argv
from math import trunc, exp, sqrt
import matplotlib.pyplot as plt

e = 2.71828182
t = -2
u = 0.71
lambda_ = 0.024
m = 1

def Factorial(n):
    factorial = 0
    if (n == 0):
        factorial = 1
    else:
        factorial = 1
        for i in range(1,n+1):
            factorial *= i
    return factorial

def Delta(n):
    ret_value = -1
    if n >= 0:
        ret_value = 1
    elif n < 0:
        ret_value = 0
    return ret_value

def find_p(index, N, lambda_, u):
    sum = 0
    for l in range(N+1):
        sum += (pow(u / lambda_, l) * (1.0 / Factorial(l)))
    return pow((u / lambda_), index) * (1.0 / Factorial(index)) / trunc(sum)

def find_ul(i, l, u, t, N, m, e):
    return pow(u * t, l) / Factorial(l) * trunc(Delta(N - i - m) * pow(m, l) * pow(e, -m * u * t)
            + Delta(m - N + i) * pow(N - i, l) * pow(e, -(N - i) * u * t))

def U(i, l, u, t, N, m):
    U = pow(u * t, l) / Factorial(l)
    U *= Delta(N - i - m) * pow(m, l) * exp(-1 * m * u * t) + Delta(m - N + i) * pow(N - i, l) * exp(-1 * (N - i) * u * t)
    return U

def find_pir(i, r, lambda_, t, e):
    return pow(i * lambda_ * t, r) / Factorial(r) * pow(e, -1 * i * lambda_ * t)

def S(m, N, n, lambda_, u):
    if m == N:
        power = N - n + 1
        return 1 - pow(lambda_, power) * pow(lambda_ + u, -power)
    result = 0.0
    for i in range(n, N+1):
        result += P1(i, N, u, lambda_)
    return result

def P1(i, N, u, lambda_):
    u_lam = u / lambda_
    P = pow(u_lam, i) * (1 / fact(i))
    sum = 0
    for l in range(N+1):
        sum += pow(u_lam, l) * (1 / fact(l))
    P *= 1 / sum
    return P

def fact(value):
    if value == 0 or value == 1:
        return 1
    return sqrt(2 * 3.1415 * value) * pow(value / e, value)

def main(argc):
    global e, t, u, lambda_, m
    match argc[1]:
        case "1":
            N = 10
            n = 8
            arr = [[] for _ in range(3)]
            for current_n in range(n, N+1):
                t = 0 # Правильная инициализация переменной t перед использованием.
                for k in range(12+1):
                    t += 2 # Предполагаем, что это корректное увеличение переменной t.
                    operationalReliability = 0 # Обнуляем перед новым набором расчетов.
                    for i in range(current_n, N+1):
                        p = find_p(i, N, lambda_, u)
                        q = 0
                        for l in range(N+1):
                            ul = find_ul(i, l, u, t, N, m, e)
                            pir = 0
                            for r in range(i-current_n+l+1):
                                pir += find_pir(i, r, lambda_, t, e)
                            q += ul * pir # Суммируем вероятности для текущего i.
                        operationalReliability += p * q # Умножаем на вероятность безотказной работы и суммируем.
                    operationalReliability = min(operationalReliability, 1) # Ограничиваем значение одной единицей.
                    print(operationalReliability)
                    arr[current_n % 3].append(operationalReliability)
                t = -2
            label = [f"n = {str(i)}" for i in range(8, 10+1)]
            xs = [i for i in range(0,25,2)]
            pairs = [[xs_, a1] for arr_ in arr for xs_, a1 in zip(xs, arr_)]
            points = [i for i in range(0, len(pairs)+1, len(pairs)//3)]
            x = []
            y = []
            for z in range(0, 3):
                x.append([pairs[i][0] for i in range(points[z], points[z+1])])
                y.append([pairs[i][1] for i in range(points[z], points[z+1])])
            fig, ax = plt.subplots()
            l = 0
            for x_, y_ in zip(x, y):
                plt.plot(x_, y_, label=label[l])
                l+=1
            ax.legend()
            ax.grid(True)
            plt.title("График зависимости функции оперативной надежности для\nN = 10; n ∈{8, 9, 10}; λ = 0,024 1/ч; µ = 0,71 1/ч; m = 1; t = 0, 2, 4, …, 24 ч", fontsize=10)
            ax.set_xlabel("Время (ч)", fontsize=10)
            ax.set_ylabel("R*(t)", fontsize=10)
            fig.savefig(f"2.png")
            plt.close(fig)
        case "2":
            N = 16
            n = 10
            arr = [[] for _ in range(7)]
            for _ in range(12+1):
                t += 2
                for a in range(10, 16+1):
                    n = a
                    operational_recoverability = 0
                    for i in range(n-1+1):
                        p = find_p(i, N, lambda_, u)
                        sum = 0
                        ul = 0
                        for r in range(n-i+1):
                            pir = find_pir(i, r, lambda_, t, e)
                            for l in range(n-i-1+r+1):
                                ul += U(i, l, u, t, N, m)
                            sum += pir * ul
                        operational_recoverability += p * sum
                    arr[a%10].append(1.0-operational_recoverability)
            label = [f"n = {str(i)}" for i in range(10, 16+1)]
            xs = [i for i in range(0,25,2)]
            pairs = [[xs_, a1] for arr_ in arr for xs_, a1 in zip(xs, arr_)]
            points = [i for i in range(0, len(pairs)+1, len(pairs)//7)]
            x = []
            y = []
            for z in range(0, 7):
                x.append([pairs[i][0] for i in range(points[z], points[z+1])])
                y.append([pairs[i][1] for i in range(points[z], points[z+1])])
            fig, ax = plt.subplots()
            l = 0
            for x_, y_ in zip(x, y):
                plt.plot(x_, y_, label=label[l])
                l+=1
            plt.title("График зависимости функции оперативной восстановимости для\nN = 16; n ∈{10, 11, …, 16}; λ = 0,024 1/ч; µ = 0,71 1/ч; m = 1; t = 0, 2, 4, …, 24 ч", fontsize=10)
            ax.set_xlabel("Время (ч)", fontsize=10)
            ax.set_ylabel("U*(t)", fontsize=10)
            ax.legend()
            ax.grid(True)
            fig.savefig(f"3.png")
            plt.close(fig)
        case "3":
            N = 16
            n = 11
            arr = [["n", "m = 1", "m = 16"]]
            nums = [str(i) for i in range(11, 16+1)]
            tmp = []
            for _ in range(1+1):
                for i in range(11, 16+1):
                    n = i
                    tmp.append(S(m, N, n, lambda_, u))
                m = 16
            for i in range(6):
                arr.append([nums[i], tmp[i], tmp[i+6]])
            fig, ax = plt.subplots()
            table = ax.table (cellText=arr, cellLoc='center', loc='center')
            table.scale(1.5, 4)
            ax.axis('off')
            fig.savefig(f"4.png", bbox_inches='tight')
            plt.close(fig)

if __name__ == "__main__":
    main(argv)