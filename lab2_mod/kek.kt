@file:JvmName("JDoodle")
import StructuralRedundancy.OperationalReliability.pj
import StructuralRedundancy.OperationalReliability.pj1
import StructuralRedundancy.OperationalReliability.ul
import kotlin.math.exp
import kotlin.math.pow
import java.math.BigInteger
import java.util.*

inline fun inputData(count: Int, inputted: (Int) -> Unit) {
    while (true) {
        try {
            print("Введите номер операции (1-$count): ")
            val number = Scanner(System.`in`).next().toInt()
            if ((1..count).contains(number)) {
                inputted(number)
            } else throw IllegalStateException("Нет такой операции")
        } catch (ex: NumberFormatException) {
            println("Неверный ввод")
        } catch (ex: IllegalStateException) { println(ex.localizedMessage) }
    }
}

fun List<List<Double>>.show() = map { it.toColumn() }
    .reduce { i, j -> "$i\n\n$j" }

private fun List<Double>.toColumn() = map { String.format("%.9f", it).replace('.', ',') }.reduce { i, j -> "$i\n$j" }//map { String.format("%.3f", it).replace('.', ',') }


inline fun pow(i: Int, j: Int, pow: (l: Int) -> Double) = (i..j).map(pow).reduce { h, k -> h * k }
inline fun sum(i: Int, j: Int, sum: (j: Int) -> Double) = (i..j).map(sum).sum()
fun Int.pow(x: Int) = toDouble().pow(x)
fun delToFact(del: Double, fact: Int) = (del.toBigDecimal() / fact(fact).toBigDecimal()).toDouble()

fun fact(num: Int): BigInteger {
    var factorial = BigInteger.ONE
    for (i in 1..num) {
        factorial = factorial.multiply(BigInteger.valueOf(num.toLong()))
    }
    return factorial
}

/**
 * Распределенные вычислительные системы со структурной избыточностью
 */
object StructuralRedundancy {
    /** Функция R*(t) оперативной надежности */
    object OperationalReliability {

        fun calculate(N: Int, λ: Double, m: Int, n: Int, µ: Double, t: Int) =
            sum(n, N) { i -> pj(N, λ, µ, i) * qi(N, λ, m, n, µ, t, i) }

        fun pj(N: Int, λ: Double, µ: Double, j: Int) =
            pj1(λ, µ, j) * sum(0, N) { l ->  pj1(λ, µ, l) }.pow(-1)

        fun pj1(λ: Double, µ: Double, i: Int) = delToFact((µ / λ).pow(i), i)

        private fun qi(N: Int, λ: Double, m: Int, n: Int, µ: Double, t: Int, i: Int) =
            sum(0, N) { l -> ul(N, m, µ, t, i, l) * sum(0, i - n + l) { r -> pi(i,  λ, t, r) } }

        private fun pi(i: Int, λ: Double, t: Int, r: Int) =
            delToFact((i * λ * t).pow(r), r) * exp(-i * λ * t)

        fun ul(N: Int, m: Int, µ: Double, t: Int, i: Int, l: Int) =
            delToFact((µ * t).pow(l), l) * (delta(N - i - m) * m.pow(l) * exp(-m * µ * t) +
                delta(m - N + i) * (N - i).pow(l) * exp(-(N - i) * µ * t))

        private fun delta(x: Int) = if (x < 0) 0 else 1
    }

    /** Функция U*(t) оперативной восстановимости */
    fun operationalRecovery(N: Int, λ: Double, m: Int, n: Int, µ: Double, t: Int): Double =
        1.0 - (sum(0, n - 1) { i ->
            pj(N, λ, µ, i) * sum(0, n - 1 - i) { l -> ul(N, m, µ, t, i, l) } })

    /** Коэффициент S готовности */
    fun availabilityFactor(N: Int, λ: Double, m: Int, n: Int, µ: Double) = when (m) {
        1 ->  1.0 - sum(0, n - 1) { i -> pj(N, λ, µ, i)* sum(0, n - 1 - i) { l -> ul(N, m, µ, 0, i, l) } }
        N -> 1.0 - (λ.pow(N - n  + 1) * (λ + µ).pow(-(N - n + 1)))
        else -> throw IllegalStateException("Невозможное условие")
    }

    fun pjm(m: Int, λ: Double, µ: Double, j: Int) = delToFact(((m * µ) / λ).pow(j) * exp((-µ * m) / λ), j)
}

object Laba2 {
    fun run() {
        inputData(3) {
            val result = when (it) {
                1 -> {
                    println("\n2. Функция  R*(t) оперативной надежности: N = 10; n ∈{8, 9, 10}; λ = 0,024 1/ч; µ = 0,71 1/ч; m = 1; t = 0, 2, 4, …, 24 ч.\n")
                    result1()
                }
                2 -> {
                    println("\n3. Функция U*(t) оперативной восстановимости: N = 16; n ∈{10, 11, …, 16}; λ = 0,024 1/ч; µ = 0,71 1/ч; m = 1; t = 0, 2, 4, …, 24 ч.\n")
                    result2()
                }
                3 -> {
                    println("\nКоэффициент S готовности: N = 16; λ = 0,024 1/ч; µ = 0,71 1/ч.\n")
                    result3()
                }
                else -> throw IllegalStateException("Нет такой операции")
            }
            println(result.show() + "\n\n")
        }
    }

    private fun result1() = (8..10).map { n ->
        (0..24 step 2).map {  t ->
            StructuralRedundancy.OperationalReliability
                .calculate(N = 10, λ =  0.024, m = 1, n = n, µ = 0.71, t = t)
        }
    }

    private fun result2() = (10..16).map { n ->
        (0..24 step 2).map {  t ->
            StructuralRedundancy.operationalRecovery(N = 16, λ =  0.024, m = 1, n = n, µ = 0.71, t = t)
        }
    }

    private fun result3() = listOf(1, 16).map { m ->
        (11..16).map { n ->
            StructuralRedundancy.availabilityFactor(N = 16, λ =  0.024, m = m, n = n, µ = 0.71)
        }
    }
}

fun main() { Laba2.run() }
