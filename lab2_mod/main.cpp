#include <iostream>
#include <cmath>
#include <vector>
#include <stdexcept>

double pj1(double lambda, double mu, int i) {
  return delToFact(std::pow(mu / lambda, i), i);
}

double pj(int N, double lambda, double mu, int j) {
  double sum_pj1 = 0.0;
  for (int l = 0; l <= N; ++l) {
    sum_pj1 += pj1(lambda, mu, l);
  }
  return pj1(lambda, mu, j) * std::pow(sum_pj1, -1);
}

double pi(int i, double lambda, int t, int r) {
  return delToFact(std::pow(i * lambda * t, r), r) * std::exp(-i * lambda * t);
}

double ul(int N, int m, double mu, int t, int i, int l) {
  return delToFact(std::pow(mu * t, l), l) * (
                                                delta(N - i - m) * std::pow(m, l) * std::exp(-m * mu * t) +
                                                delta(m - N + i) * std::pow(N - i, l) * std::exp(-(N - i) * mu * t)
                                            );
}

double qi(int N, double lambda, int m, int n, double mu, int t, int i) {
  double sum_ul_pi = 0.0;
  for (int l = 0; l <= N; ++l) {
    sum_ul_pi += ul(N, m, mu, t, i, l) * sum(0, i - n + l, [&](int r) { return pi(i, lambda, t, r); });
  }
  return sum_ul_pi;
}

double operationalRecovery(int N, double lambda, int m, int n, double mu, int t) {
  double result = 1.0;
  for (int i = 0; i < n; ++i) {
    result -= pj(N, lambda, mu, i) * sum(0, n - 1 - i, [&](int l) { return ul(N, m, mu, t, i, l); });
  }
  return result;
}

double availabilityFactor(int N, double lambda, int m, int n, double mu) {
  switch (m) {
  case 1:
  {
    double result = 1.0;
    for (int i = 0; i < n; ++i) {
      result -= pj(N, lambda, mu, i) * sum(0, n - 1 - i, [&](int l) { return ul(N, m, mu, 0, i, l); });
    }
    return result;
  }
  case N:
    return 1.0 - (std::pow(lambda, N - n + 1) * std::pow(lambda + mu, -(N - n + 1)));
  default:
    throw std::invalid_argument("Невозможное условие");
  }
}

double pjm(int m, double lambda, double mu, int j) {
  return delToFact(std::pow(m * mu / lambda, j) * std::exp(-mu * m / lambda), j);
}

inline double pow(int i, int j, std::function<double(int)> pow_func) {
  double result = 1.0;
  for (int k = i; k <= j; ++k) {
    result *= pow_func(k);
  }
  return result;
}

inline double sum(int i, int j, std::function<double(int)> sum_func) {
  double result = 0.0;
  for (int k = i; k <= j; ++k) {
    result += sum_func(k);
  }
  return result;
}

double delToFact(double del, int fact) {
  return del / fact(fact);
}

long long fact(int num) {
  long long factorial = 1LL;
  for (int i = 1; i <= num; ++i) {
    factorial *= i;
  }
  return factorial;
}

inline int delta(int x) {
  return (x < 0) ? 0 : 1;
}

int input() {
  int input;
  int min = 1;
  int max = 3;
  while (true) {
    std::cout << "Выберите пункт меню: ";
    std::cin >> input;

    if (std::cin.fail() || input < min || input > max) {
      std::cin.clear(); // Сброс ошибки ввода
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // Очистка буфера ввода
      std::cout << "Ошибка: введите корректный пункт меню." << std::endl;
    } else {
      break;
    }
  }
  return input;
}

class Laba2 {
public:
  int run() {
    int ans = input();
    switch(ans) {
    case 1:
      printf("\n1. Функция  R*(t) оперативной надежности: N = 10; n ∈{8, 9, 10};"
             "λ = 0,024 1/ч; µ = 0,71 1/ч; m = 1; t = 0, 2, 4, …, 24 ч.\n");
      result1();
      break;
    case 2:
      println("\n2. Функция U*(t) оперативной восстановимости: N = 16; "
              "n ∈{10, 11, …, 16}; λ = 0,024 1/ч; µ = 0,71 1/ч; m = 1; "
              "t = 0, 2, 4, …, 24 ч.\n");
      result2();
      break;
    case 3:
      println("\n3. Коэффициент S готовности: N = 16; λ = 0,024 1/ч; µ = 0,71 1/ч.\n");
      result3();
      break;
    default:
      // code block
    }
    // result.show();
  }
private:
  void result1() {
    for (int n = 8; n <= 10; n++) {
      for (int t = 0; t <= 24; t += 2) {
        // N = 10, λ =  0.024, m = 1, n = n, µ = 0.71, t = t
        StructuralRedundancy.OperationalReliability.calculate(10, 0.024, 1, n, 0.71, t);
      }
    }
  }

  void result2() {
    for (int n = 10; n <= 16; n++) {
      for (int t = 0; t <= 24; t += 2) {
        // (N = 16, λ =  0.024, m = 1, n = n, µ = 0.71, t = t)
        StructuralRedundancy.operationalRecovery(16, 0.024, 1, n, 0.71, t);
      }
    }
  }

  void result3() {
    for (int m = 1; m <= 16; m += 15) {
      for (int n = 11; n <= 16; n++) {
        // (N = 16, λ =  0.024, m = m, n = n, µ = 0.71)
        StructuralRedundancy.availabilityFactor(16, 0.024, m, n, 0.71);
      }
    }
  }
};

int main() {
  Laba2 a;
  a.run();
  return 0;
}


//int main() {
//  // Пример использования функций
//  int N = 10, m = 5, n = 3, t = 2;
//  double λ = 0.1, µ = 0.05;
//
//  std::cout << "Operational Reliability: " << OperationalReliability::calculate(N, λ, m, n, µ, t) << std::endl;
//  std::cout << "Operational Recovery: " << operationalRecovery(N, λ, m, n, µ, t) << std::endl;
//  std::cout << "Availability Factor: " << availabilityFactor(N, λ, m, n, µ) << std::endl;
//  std::cout << "pjm: " << pjm(m, λ, µ, 3) << std::endl;
//
//  return 0;
//}


