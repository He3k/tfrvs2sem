set terminal png size 1200, 900 font 'Verdana, 14'
set title "U*(t) (N = 16; n ∈{10, 11, …, 16}; λ = 0,024 1/ч; µ = 0,71 1/ч; m = 1; t = 0, 2, 4, …, 24 ч. )" font "Helvetica Bold, 18"
set output "3.png"

set grid
set key right top

set xrange [0:24]
set xlabel "t"
set xtics 2

set yrange [0:1]
set ylabel "U*(T)"

#set logscale y 2

plot '3.txt' u 1:2 with linespoints lw 3 pt 8 ps 1 title 'n = 10',\
     '3.txt' u 1:3 with linespoints lw 3 pt 7 ps 1 title 'n = 11',\
     '3.txt' u 1:4 with linespoints lw 3 pt 6 ps 1 title 'n = 12',\
     '3.txt' u 1:5 with linespoints lw 3 pt 7 ps 1 title 'n = 13',\
     '3.txt' u 1:6 with linespoints lw 3 pt 7 ps 1 title 'n = 14',\
     '3.txt' u 1:7 with linespoints lw 3 pt 7 ps 1 title 'n = 15',\
     '3.txt' u 1:8 with linespoints lw 3 pt 7 ps 1 title 'n = 16'
