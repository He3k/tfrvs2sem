import matplotlib.pyplot as plt
from time import time
import os

def mul(N, l, m, mu):
	if (N - m <= l and l <= N):
		return mu * (N - l)
	else:
		return m * mu

def Theta(N, n, m, mu, _lambda):
	theta = 0.0
	if n==N:
		return 1/(n * _lambda)
	for j in range(n+1, N+1):
		res = 1.0
		for l in range(n, j):
			res *= ((mul(N, l, m, mu) / (l * _lambda)))
		theta += (1 / (j * _lambda)) * res
		theta += 1 / (n * _lambda)
	return theta

def T(N, n, m, mu, _lambda):
    T_val = 1 / mu
    for l in range(1, n):
        T_val *= (l * _lambda) / mul(N, l, m, mu)
    for j in range(1, n):
        res = 1
        for l in range(j, n):
            res *= (l * _lambda) / mul(N, l, m, mu)
        T_val += (1 / (j * _lambda)) * res
    return T_val

def make_graph(num, arr, n, param):
	fig, ax = plt.subplots()
	match num:
		case 1:
			for i, sub_arr in enumerate(arr):
				ax.plot(n, sub_arr, label=f"μ = {param[i]} 1/hours")
			title = "Mean time between failures (N = 65536, λ = 10^-5 hours^-1, m = 1)"
			xl = "Number n of elementary machines in base subsystem"
			yl = "Meen time between failures (hours)"
			filename = "2_1.png"
		case 2:
			for i, sub_arr in enumerate(arr):
				ax.plot(n, sub_arr, label=f"λ = {param[i]} 1/hours")
			title = "Mean time between failures (N = 65536, μ = 1 hours^-1, m = 1)"
			xl = "Number n of elementary machines in base subsystem"
			yl = "Meen time between failures (hours)"
			filename = "2_2.png"
		case 3:
			for i, sub_arr in enumerate(arr):
				ax.plot(n, sub_arr, label=f"m = {param[i]}")
			title = "Mean time between failures (N = 65536, λ = 10^-5 hours^-1, μ = 1 hours^-1)"
			xl = "Number n of elementary machines in base subsystem"
			yl = "Meen time between failures (hours)"
			filename = "2_3.png"
		case 4:
			for i, sub_arr in enumerate(arr):
				ax.plot(n, sub_arr, label=f"μ = {param[i]} hours^-1")
			title = "Mean time to recovery (N = 1000, λ = 10^-3 hours^-1, m = 1)"
			xl = "Number n of elementary machines in base subsystem"
			yl = "Meen time to recovery (hours)"
			filename = "3_1.png"
		case 5:
			for i, sub_arr in enumerate(arr):
				ax.plot(n, sub_arr, label=f"λ = {param[i]} hours^-1")
			title = "Mean time to recovery (N = 8192, μ = 1 hours^-1, m = 1)"
			xl = "Number n of elementary machines in base subsystem"
			yl = "Meen time to recovery (hours)"
			filename = "3_2.png"
		case 6:
			for i, sub_arr in enumerate(arr):
				ax.plot(n, sub_arr, label=f"m = {param[i]}")
			title = "Mean time to recovery (N = 8192, λ = 10^-5 hours^-1, μ = hours^-1)"
			xl = "Number n of elementary machines in base subsystem"
			yl = "Meen time to recovery (hours)"
			filename = "3_3.png"
	plt.title(title, fontsize=10)
	ax.set_xlabel(xl, fontsize=10)
	ax.set_ylabel(yl, fontsize=10)
	ax.legend()
	ax.set_xlim(min(n), max(n))
	ax.grid(True)
	plt.yscale('log')
	fig.savefig(f"graphs/{filename}", dpi=100, bbox_inches='tight')
	plt.close(fig)

def write_arr(m, arr):
	with open("arrs.dat", "a+") as file:
		file.write(f"{m} {arr}\n")

def two_one():
	N = 65536
	_lambda = pow(10, -5)
	m = 1
	n = [i for i in range(65527, 65536+1)]
	mu = [pow(10, j) for j in range(0,3+1)]
	arr = []
	for _mu in mu:
		sub_arr = []
		for _n in n:
			sub_arr.append(Theta(N, _n, m, _mu, _lambda))
		arr.append(sub_arr)
	write_arr("Массив значений для 2.1:", arr)
	make_graph(1, arr, n, mu)

def two_two():
	N = 65536
	_lambda = [pow(10, i) for i in range(-5, -10, -1)]
	m = 1
	n = [i for i in range(65527, 65536+1)]
	mu = 1
	arr = []
	for __lambda in _lambda:
		sub_arr = []
		for _n in n:
			sub_arr.append(Theta(N, _n, m, mu, __lambda))
		arr.append(sub_arr)
	write_arr("Массив значений для 2.2:", arr)
	make_graph(2, arr, n, _lambda)

def two_three():
	N = 65536
	_lambda = pow(10, -5)
	m = [i for i in range(1, 4+1)]
	n = [i for i in range(65527, 65536+1)]
	mu = 1
	arr = []
	for _m in m:
		sub_arr = []
		for _n in n:
			sub_arr.append(Theta(N, _n, _m, mu, _lambda))
		arr.append(sub_arr)
	write_arr("Массив значений для 2.3:", arr)
	make_graph(3, arr, n, m)

def three_one():
	N = 1000
	_lambda = pow(10, -3)
	m = 1
	n = [i for i in range(900, 1000+1, 10)]
	mu = [1, 2, 4, 6]
	arr = []
	for _mu in mu:
		sub_arr = []
		for _n in n:
			sub_arr.append(T(N, _n, m, _mu, _lambda))
		arr.append(sub_arr)
	write_arr("Массив значений для 3.1:", arr)
	make_graph(4, arr, n, mu)

def three_two():
	N = 8192
	_lambda = [pow(10, i) for i in range(-5, -10, -1)]
	m = 1
	n = [i for i in range(8092, 8192+1, 10)]
	mu = 1
	arr = []
	for __lambda in _lambda:
		sub_arr = []
		for _n in n:
			sub_arr.append(T(N, _n, m, mu, __lambda))
		arr.append(sub_arr)
	write_arr("Массив значений для 3.2:", arr)
	make_graph(5, arr, n, _lambda)

def three_three():
	N = 8192
	_lambda = pow(10, -5)
	m = [i for i in range(1, 4+1)]
	n = [i for i in range(8092, 8192+1, 10)]
	mu = 1
	arr = []
	for _m in m:
		sub_arr = []
		for _n in n:
			sub_arr.append(T(N, _n, _m, mu, _lambda))
		arr.append(sub_arr)
	write_arr("Массив значений для 3.3:", arr)
	make_graph(6, arr, n, m)

if __name__ == "__main__":
	if not os.path.exists("graphs"):
		os.mkdir("graphs")
	start = time()
	two_one()
	two_two()
	two_three()
	# three_one()
	# three_two()
	# three_three()
	print(f"Построение графиков завершено за {time()-start} секунд")